# Projeto Modelo - NextJs com Tema Dark e Light.

## Tecnologias e Bibliotecas Utilizadas.

- [ReactJS](https://nextjs.org/)
- [React DOM](https://nextjs.org/)
- [Next.js](https://nextjs.org/)
- [Styled Components](https://nextjs.org/)
- [babel-plugin-styled-components](https://nextjs.org/)

> Criei um projeto Base onde no mesmo vc pode usar o Tema Light e Dark e o projeto possui as seguintes Funcionalidades :
>
> - Temas Dark e Light;
> - Arquivo de configuração dos temas;
> - Possui GlobalStyle, onde é definido um estilo principal para a Aplicação;
> - Utiliza ThemeProvider para gerenciar os temas;
> - Utiliza LocalStorage para Salvar o ultimo tema utilizado e não o perde na atualização da Aplicação;
> - Possui um botão de demonstração para chavear a aplicação entre o Dark Mode e o Light Mode;
> - Possui Plugin do babel para styled component que integrado ao next nos ajuda da seguinte forma :
>   - Pacotes menores
>   - Compatibilidade de renderização do lado do servidor
>   - Melhor depuração
>   - Minificação
>   - Eliminação de código morto

## Iniciando o Projeto

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.
