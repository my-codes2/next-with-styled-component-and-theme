import React, { useState, useEffect } from "react";
import { ThemeProvider } from "styled-components";

import { GlobalStyles } from "../styles/global";
import { lightTheme, darkTheme } from "../styles/themes";

function Layout({ children }) {
  // Usa o state para definir o dark mode
  const [darkMode, setDarkMode] = useState();
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    const darkModeValue = localStorage.getItem("DARK_MODE");
    // a LocalStorage retorna string e não booleano
    setDarkMode(darkModeValue === "true");
    // Defina mounted como `true` somente após definir o estado` darkMode`
    setMounted(true);
  }, []);

  useEffect(() => {
    localStorage.setItem("DARK_MODE", darkMode);
  }, [darkMode]); //Executar sempre que o DarkMode mudar

  if (!mounted) return <div />;

  return (
    // Isso irá passar `theme` de` ThemeProvider` como um prop para `GlobalStyles`
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <div>
        <button onClick={() => setDarkMode(!darkMode)}>
          {darkMode ? "Modo Light" : "Modo Dark"}
        </button>
      </div>

      <GlobalStyles />
      {children}
    </ThemeProvider>
  );
}

export default Layout;
