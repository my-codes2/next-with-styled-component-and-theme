import React from "react";
import Layout from "./../components/Layout";

export default function Home() {
  return (
    <Layout>
      <div>
        <h1>Modelo NextJs com Styled Component com Temas Light e Dark</h1>
      </div>
    </Layout>
  );
}
