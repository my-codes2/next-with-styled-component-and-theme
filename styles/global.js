import { createGlobalStyle } from "styled-components";

// o objeto `theme` está vindo de nosso arquivo ./themes.js
export const GlobalStyles = createGlobalStyle`
  body {
    background-color: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text}
  }
`;
